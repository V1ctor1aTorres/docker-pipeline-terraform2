# Projeto Docker-Pipeline-Terraform2
Simulação onde o profissional de Devops tem acesso ao código fonte de uma aplicação web, podendo buildar a imagem e executá-la em um Container Docker na AWS.

## Subir o servidor Docker com o Terraform e conectá-lo à um repositório no Gitlab:
- Criar os arquivos de configuração do Terraform: **main.tf**, **ec2.tf**, **securitygroup.tf** e **script.sh**.
- Criar um repositório no Gitlab com os arquivos Terraform, e vincular ao servidor, através do arquivo script.sh.
- Aplicar os comandos **terraform init**, **terraform plan** e **terraform  apply**, para criar a infraestrutura.
- Na console AWS, conectar-se à EC2 criada e verificar se o Docker foi instalado (**sudo docker –version**), se  o gitlab runner foi instalados (**cat /etc/passwd**), e se o gitlab runner tem a permissão necessária para executar o Docker (**cat /etc/group**).
- No mesmo diretório do Terraform, criar uma pasta contendo o código da aplicação e o Dockerfile.

## Construir o pipeline:
- Criar o arquivo de pipeline, **.gitlab-ci.yml**, para criar a imagem, fazer upload para o Dockerhub, baixar a imagem e executar no servidor na AWS.
- Não esquecer de fazer o login no Docker com variáveis, por segurança.
- Podemos também criar o arquivo **.gitignore**, usado para não subir certos trechos do seu projeto para o repositório.

## Testar se a aplicação está disponível no servidor na AWS, verificando o endereço IPv4 público da EC2:
Caso o código da aplicação seja alterado, podemos atualizar a versão e disponibilizar para o usuário em um único pipeline.
![EC2](./images/ec2.png)

![site](./images/site.png)

![pipelines](./images/pipelines.png)



